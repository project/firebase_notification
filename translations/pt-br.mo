��          �   %   �      @  	   A     K  $   X     }     �  %   �     �     �     �     �     	       9   ;     u     z  
   �  �   �     T     r  �   �  )   R     |     �     �  ]  �  
   �     �  -        2  )   J  6   t     �     �     �     �     �  !   �  C        ]     d     z  �   �  "   m	     �	  �   �	  >   �
     �
     �
     �
                         	                                                                 
                                    Condition Content Type Default value for notification title Firebase Notification Firebase Notification Settings Manage firebase notification settings Message Mobile Notification No Notification Test Notification Title Notification sent successfully Select the content types you want to enable notifications Send Send Mobile Notification? Server Key Supported condition: Topic, formatted as "'yourTopic' in topics". This value is case-insensitive.<br>Supported operators: &&, ||. Maximum two operators per topic message supported. The notification's body text. The notification's title. The value can be a device's registration token, a device group's notification key, or a single topic (prefixed with <b>/topics/</b>). To send to multiple topics, use the <b>condition</b> parameter. These settings apply only to android app. Title To Yes Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2018-10-10 15:50-0300
PO-Revision-Date: 2018-10-10 15:53-0300
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.0.6
Last-Translator: 
Language: pt_BR
 Condição Tipo de Conteúdo Valor padrão para o título da notificação Notificações Firebase Configurações de Notificação Firebase Gerenciar as configurações de notificação firebase Mensagem Notificação Não Teste de Notificação Título da Notificação Notificação enviada com sucesso Selecione os tipos de conteúdo que deseja ativar as notificações Enviar Enviar Notificação? Chave do Servidor Condição aceita: tópico, formatado como "'seu tópico' in  topics". Esse valor não diferencia minúsculas de maiúsculas.<br>Operadores aceitos: &&, ||. No máximo dois operadores são permitidos por mensagem de tópico. O texto do corpo da notificação. O título da notificação. O valor pode ser o token de registro de um dispositivo, a chave da notificação de um grupo de dispositivos ou um único tópico (prefixado com <b>/topics/</b>). Para enviar para vários tópicos, use o parâmetro <b>condição</b>. Essas configurações se aplicam apenas ao aplicativo Android. Título Para Sim 