# $Id$
#
# LANGUAGE translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  firebase_notification.module: n/a
#  includes/firebase_notification.admin.inc: n/a
#  firebase_notification.info: n/a
#  includes/firebase_notification.menu.inc: n/a
#  includes/firebase_notification.permissions.inc: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2018-10-10 15:50-0300\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: firebase_notification.module:21
msgid "Mobile Notification"
msgstr ""

#: firebase_notification.module:29
msgid "Send Mobile Notification?"
msgstr ""

#: firebase_notification.module:32
msgid "No"
msgstr ""

#: firebase_notification.module:33
msgid "Yes"
msgstr ""

#: firebase_notification.module:41 includes/firebase_notification.admin.inc:26
msgid "Notification Title"
msgstr ""

#: firebase_notification.info:0
msgid "Firebase Notification"
msgstr ""

#: firebase_notification.info:0
msgid "FCM Push Notification"
msgstr ""

#: includes/firebase_notification.admin.inc:12
msgid "These settings apply only to android app."
msgstr ""

#: includes/firebase_notification.admin.inc:19
msgid "Server Key"
msgstr ""

#: includes/firebase_notification.admin.inc:27
msgid "Default value for notification title"
msgstr ""

#: includes/firebase_notification.admin.inc:33;66
msgid "To"
msgstr ""

#: includes/firebase_notification.admin.inc:34;67
msgid "The value can be a device's registration token, a device group's notification key, or a single topic (prefixed with <b>/topics/</b>). To send to multiple topics, use the <b>condition</b> parameter."
msgstr ""

#: includes/firebase_notification.admin.inc:40;72
msgid "Condition"
msgstr ""

#: includes/firebase_notification.admin.inc:41;73
msgid "Supported condition: Topic, formatted as \"'yourTopic' in topics\". This value is case-insensitive.<br>Supported operators: &&, ||. Maximum two operators per topic message supported."
msgstr ""

#: includes/firebase_notification.admin.inc:51
msgid "Content Type"
msgstr ""

#: includes/firebase_notification.admin.inc:52
msgid "Select the content types you want to enable notifications"
msgstr ""

#: includes/firebase_notification.admin.inc:59
msgid "Notification Test"
msgstr ""

#: includes/firebase_notification.admin.inc:78
msgid "Title"
msgstr ""

#: includes/firebase_notification.admin.inc:79
msgid "The notification's title."
msgstr ""

#: includes/firebase_notification.admin.inc:84
msgid "Message"
msgstr ""

#: includes/firebase_notification.admin.inc:85
msgid "The notification's body text."
msgstr ""

#: includes/firebase_notification.admin.inc:90
msgid "Send"
msgstr ""

#: includes/firebase_notification.admin.inc:120
msgid "Notification sent successfully"
msgstr ""

#: includes/firebase_notification.menu.inc:10
msgid "Firebase Notification Settings"
msgstr ""

#: includes/firebase_notification.permissions.inc:11
msgid "Manage firebase notification settings"
msgstr ""

