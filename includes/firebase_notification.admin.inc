<?php

/**
 * Form Settings
 */
function firebase_notification_settings() {
	$form = array();

	$form['android'] = array(
		'#type' => 'fieldset',
		'#title' => 'Android',
		'#description' => t('These settings apply only to android app.'),
		'#collapsible' => TRUE,
    '#collapsed' => FALSE,
	);
	
 	$form['android']['firebase_notification_server_key'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Server Key'),
 		'#default_value' => variable_get('firebase_notification_server_key'),
 		'#maxlength' => 1024,
 	);
 	
 	$form['android']['firebase_notification_title'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Notification Title'),
 		'#description' => t('Default value for notification title'),
 		'#default_value' => variable_get('firebase_notification_title'),
 	);
 	
 	$form['android']['firebase_notification_to'] = array(
 		'#type' => 'textfield',
 		'#title' => t('To'),
 		'#description' => t("The value can be a device's registration token, a device group's notification key, or a single topic (prefixed with <b>/topics/</b>). To send to multiple topics, use the <b>condition</b> parameter."),
 		'#default_value' => variable_get('firebase_notification_to'),
 	);
 	
 	$form['android']['firebase_notification_condition'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Condition'),
 		'#description' => t("Supported condition: Topic, formatted as \"'yourTopic' in topics\". This value is case-insensitive.<br>Supported operators: &&, ||. Maximum two operators per topic message supported."),
 		'#default_value' => variable_get('firebase_notification_condition'),
 	);
 	
 	foreach(node_type_get_types() as $type){
 		$content_type[$type->type] = $type->name;
 	}
 	
 	$form['android']['firebase_notification_content_type'] = array(
 		'#type' => 'checkboxes',
 		'#title' => t('Content Type'),
 		'#description' => t('Select the content types you want to enable notifications'),
 		'#default_value' => empty(variable_get('firebase_notification_content_type')) ? array() : variable_get('firebase_notification_content_type'),
 		'#options' => $content_type,
 	);
 	
 	$form['android']['notification_test'] = array(
 		'#type' => 'fieldset',
 		'#title' => t('Notification Test'),
 		'#collapsible' => TRUE,
 		'#collapsed' => TRUE,
 	);
 	
 	$form['android']['notification_test']['test_to'] = array(
 		'#type' => 'textfield',
 		'#title' => t('To'),
 		'#description' => t("The value can be a device's registration token, a device group's notification key, or a single topic (prefixed with <b>/topics/</b>). To send to multiple topics, use the <b>condition</b> parameter."),
 	);
 	
 	$form['android']['notification_test']['test_condition'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Condition'),
 		'#description' => t("Supported condition: Topic, formatted as \"'yourTopic' in topics\". This value is case-insensitive.<br>Supported operators: &&, ||. Maximum two operators per topic message supported."),
 	);
 	
 	$form['android']['notification_test']['test_title'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Title'),
 		'#description' => t("The notification's title."),
 	);
 	
 	$form['android']['notification_test']['test_text'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Message'),
 		'#description' => t("The notification's body text."),
 	);
 	
 	$form['android']['notification_test']['test_send'] = array(
 		'#type' => 'submit',
 		'#value' => t('Send'),
 		'#submit' => array(
			'firebase_notification_test_submit'
 		),
 	);

 	return system_settings_form($form);
}

/**
 * Send Notification Test
 */
function firebase_notification_test_submit($form, $form_state) {
	if (!empty($form_state['values']['test_to'])) {
		$fields['to'] = $form_state['values']['test_to'];
	}
	if (!empty($form_state['values']['test_condition'])) {
		$fields['condition'] = $form_state['values']['test_condition'];
	}
	if (!empty($form_state['values']['test_text'])) {
		$fields['notification']['body'] = $form_state['values']['test_text'];
	}
	if (!empty($form_state['values']['test_title'])) {
		$fields['notification']['title'] = $form_state['values']['test_title'];
	}
	
	if (isset($fields['to']) || isset($fields['condition']) && isset($fields['notification'])) {
		$result = firebase_notification_send($fields);
		
		if ($result) {
			drupal_set_message(t('Notification sent successfully'));
		}
		else {
			drupal_set_message($result, 'error');
		}
	}
}