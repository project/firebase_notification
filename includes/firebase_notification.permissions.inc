<?php

define('FIREBASE_NOTIFICATION_PERMISSION_MANAGE_SETTINGS', 'firebase_notification_manage_settings');

/**
 * Implements hook_permission().
 */
function firebase_notification_permission() {
  return array(
  	FIREBASE_NOTIFICATION_PERMISSION_MANAGE_SETTINGS => array(
  		'title' => t('Manage firebase notification settings'),
  	),
  );
}