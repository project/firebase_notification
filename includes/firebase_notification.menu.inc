<?php

/**
 * Implements hook_menu().
 */
function firebase_notification_menu() {
	$paths = array();
	
	$paths['admin/config/services/firebase-notification'] = array(
		'title' => t('Firebase Notification Settings'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('firebase_notification_settings'),
		'access callback' => 'firebase_notification_settings_access_callback',
		'file' => 'includes/firebase_notification.admin.inc',
		'type' => MENU_NORMAL_ITEM,
	);
	
	return $paths;
}

/**
 * Access Callback.
 */
function firebase_notification_settings_access_callback() {
	global $user;

	$allowed = FALSE;

	if (user_access(FIREBASE_NOTIFICATION_PERMISSION_MANAGE_SETTINGS)) {
		$allowed = (bool) get_object_vars(user_load($user->uid));
	}

	return $allowed;
}