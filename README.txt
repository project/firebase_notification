This module integrates with Google's Firebase Cloud Messaging to send notifications to Android devices after creating or editing a node.

To configure is simple, just create a Firebase account and go to the module settings to set the server key, the types of content you want to send notifications and other information about the notification.

Key Features:
- Send test notifications on the settings screen.
- Select whether or not to send the notification in the node's publication screen.
- Customize the title of the notification in the node publication screen.
- Customize notification values using node fields through hook_firebase_notification_values_alter ($ node, $ notification_values)

By default the notification title is a value that has been set in the module settings (or node publishing) and the notification description is the node title value.
